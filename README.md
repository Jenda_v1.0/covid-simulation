# covid-simulation

Semestrální projekt **simulace šíření covid-19** na předmět VI1 (Výpočetní inteligence 1).



# Prostředí (/ model)

* **1 tick** reprezentuje **1 hodinu**.

* V modelu je k dispozici 6 bytů (/ _domů_).
* Každý z nich reprezentuje domov pro určitý (/ _nastavitelný_) počet lidí (/ _agentů_).
* Můžeme si dle potřeby nastavit libovolnou barvu pozadi modelu a barvu jednotlivých domů.
* K dispozici jsou komponenty pro nastavení počtu lidi / agentů, kteří se mají vygenerovat (/ _vytvořit_) v simulaci.
    * Podle proměnné `generate-random-people-position?` budou agenti při inicializaci simulace buď náhodně rozmístěni po prostředí (`true`) nebo budou umístěni před svým domem (`false`).
        * V případě **náhodného** umístění bude vždy vygenerována náhodná pozice, která bude kdekoliv po prostředí s **výjimkou domu**, do kterého agent patří (`aby byl vidět`) a mimo překážku (_aby agent nebyl umístěn například uprostřed překážky_).
        * Toto je _lepší_ (**výchozí**) varianta, protože v případě, že je alespoň jeden agent ze skupiny agentů na stejném místě nakažen, s největší pravděpodobností se nakazí všichni. Díky náhodnému rozmístění se tomu předejde.
    * Je možné náhodně vygenerovat **počet** agentů pro každý dům.
* K dispozici jsou vodorovné a svislé překážky.
    * Můžeme nastavit, že nechceme žádné překážky v modelu, že chceme pouze vodorovné, pouze svislé nebo vodorovné a svislé, viz sekce **Překážky**.
* Je možné nastavit pevný nebo náhodný počet nakažených agentů (`random-number-of-infected?`) a jejich úroveň nakažení (hodnotu `covid`).
    * Tedy, při inicializaci modelu bude mít nastavený či vygenerovaný počet agentů stejnou nebo různou (/ _náhodnou_) míru nakažení.
        * Pokud bude proměnná `randomly-generate-infection-rates?` nastavena na `true`, příslušnému počtu agentů, kteří mají být nakaženi bude náhodně vygenerována hodnota `covid`.
        * Pokud bude uvedená proměnná nastavena na `false`, agentům, kteří mají být infikování bude nastavena hodnota `covid` na hodnotu nastavenou v proměnné `initial-infectivity-rate`.
* Agent může být nakažen nemocí / virem covid-19, viz kapitola **Covid**.
    * Buď je při vytvoření (/ _inicializaci_) simulace (/ _modelu_) nastaven jako nakažený.
    * Agent může být nakažen tak, že je v přítomnosti jiného agenta po dostatečně dlouhou dobu.
    * Agent může být vyléčen tak, že půjde domů (_když má hodnotu `covid > 0`_), kde zůstane dokud nebude vyléčen. Popř. může zemřít na následky viru.
* Agentovi je přiřazeno / vygenerováno pohlaví, viz kapitola **Pohlaví**.
* Agentovi je přiřazena rouška na základě uživatelem nastavených parametrů a jeho pohlaví, viz kapitola **Rouška**.

* V gui jsou k dispozici různé monitory a grafy pro sledování aktuálního vývoje simulace.
    * Je zde uveden graf, který znázorňuje vývoj viru, resp. nakažení a úmrtí agentů kvůli viru.
    * Monitory pro zobrazení počtu žen a mužů v modelu.
    * Monitory pro zobrazení počtu aktuálně zdravých agentů (bez viru). Počet agentů, kteří jsou infikování virem. Agenti, kteří jsou infekční a agenti, kteří zemřeli (_buď selhání zdraví nebo dosažení maximálního věku_).



## Věk

* Je možné nastavit přiřazení věku agentům při jejich vygenerování.
    * Výchozí věk může být vygenerován náhodně v intervalu **<1 ; 100>**.
        * Pokud nastavíme náhodné generování, je třeba nastavit výše popsaný interval.
        * Věk bude vygenerován každému agentovi náhodně v zadaném intervalu <`minimum-age` ; `maximum-age`>.
    * Výchozí věk může být **pevně definován** (`starting-age`).
        * Například, že každý agent bude při vytvoření modelu mít věk 50.
* Maximální možný věk dožití je **100** let.
    * Pokud se agent dožije tohoto věku, zemře.
* U každého agenta je počítáno _stáří_ pro každý rok (`age-tick`).
    * Při každém ticku je hodnota této proměnné navýšena o **1**.
    * Pokud hodnota dosáhne na **8760** (_jeden rok v ticích_), hodnota v proměnné bude vynulována a bude navýšeno _stáří_ agenta o 1 rok (`age`).



## Kontrola barev

* Při nastavení modelu kliknutím na tlačítko `setup` dojde (_mimo jiné_) ke kontrole unikátnosti nastavených barev.
* Následující barvy musí být unikátní.
    * Barvy domů mezi sebou musí být unikátní.
    * Barva vodorovné a svislé překážky musí být jiná než barva domů.
    * Barva pozadí nesmí být shodná s barvou domů.
    * Barva pozadí nesmí být shodná s barvou vodorovné a svislé překážky.
    
* _Barva vodorovné a svislé překážky může být shodná (výchozí)._



## Covid

* Každý agent má možnost nakazit se nemocí (/ _virem_) covid-19.
* V modelu se k tomu přistupuje jako hodnota proměnné `covid` v intervalu **<0 ; 100>**.
    * Pokud `covid > 0 && covid <= 25`, jedná se o reprezentaci tzv. **inkubační doby**. V takovém případě je agent nakažen virem, ale ještě **není infekční**. Tedy nemůže nikoho nakazit.
    * Pokud `covid > 25`, agent je nakažen a **je infekční**. Tedy může nakazit kohokoliv jiného.
* Pokud agent je nakažen, tedy `covid > 0`, podle nastavení `go-home-when-agent-is-infected?` bude buď směřovat **domů**, kde zůstane do doby, dokud se nevyléčí nebo nezemře (`true`) nebo se bude i nadále volně pohybovat po prostředí (`false`).
* Pokud se agent vyléčí, jeho pohyb bude opět náhodný, viz kapitola **Pohyb agentů** níže.
* Uživatel má možnost při nastavení modelu nastavit, kolik agentů bude nakažených.
    * **Vždy** musí být nakažen **alespoň 1 agent**.
    * Uživatel může nastavit **pevný počet** agentů, kteří budou při spuštění modelu **nakaženi** (`random-number-of-infected = false` a `starting-number-of-infected` požadovaný počet nakažených agentů).
        * `starting-number-of-infected` musí být menší nebo rovno celkovému počtu agentů v modelu.
    * Pokud bude nastaveno, že se má **počet nakažených agentů vygenerovat náhodně**, stačí switch `random-number-of-infected?` nastavit na `true`. Poté nastavit posuvník `maximum-number-of-infected` na hodnotu, která je menší nebo rovna celkovému počtu agentů v modelu.



### Nakažení

* Po spuštění modelu agent buď je (`covid > 0`) nebo není (`covid = 0`) nakažen.
* Pokud je agent nakažen.
    * Agent bude buď směřovat domů, kde nemoc přečká (`go-home-when-agent-is-infected?`) nebo se bude volně pohybovat po prostředí.
* Pokud agent není nakažen a má imunitu (`can-be-infected? = false`), inkrementuje se hodnota proměnné `can-be-infected-tick`, dokud nedosáhne požadované hodnoty, kdy agentovi imunita vyprchá a může být znovu nakažen.
* Po tom, co se agent uzdraví bude nastavena proměnná `can-be-infected?` na `false`.
    * Tím se zajistí, že agent bude vůči opětovnému nakažení imunní po určitý počet tiků (`immunity-after-healing`).
        * Výchozí počet je **168** tiků (1 týden).
        * Počet tiků, jak dlouho je agent vyléčen je ukládán do proměnné `can-be-infected-tick`.
            * Ve chvíli, kdy hodnota této proměnné dosáhne počtu nastaveného v `immunity-after-healing`, agent přestane být imunní a může být virem opět nakažen `can-be-infected? = true`.



#### Určení nakažení agenta

* Při každém ticku jsou iterování všichni agenti v modelu.
* Při každé iteraci se (**mimo jiné**) zjistí, zdali je **doma**, pokud ano, nebude se testovat, zdali je možné se nakazit.
    * Bere se v úvahu, že **doma je agent sám** a zároveň je to **zóna, kde se virus nepřenáší** a agent se nemůže nakazit.
* Pokud je agent kdekoliv _venku_ v prostředí, vyhledají se infikovaní agenti v jeho okolí (_kteří také nejsou doma - pro případ, že by byly v dosahu vyhledávání_).
    * Pokud má agent roušku (`veil? = true`), hledají se infikovaní agenti v okolí do vzdálenosti `distance-for-infection-with-veil`.
    * Pokud agent nemá roušku (`veil? = false`), hledají se infikovaní agenti v okolí do vzdálenosti `distance-for-infection-without-veil`.
* Pokud se v okolí agenta najde jiný agent, který **je infikovaný** a **nakažlivý**, aktuální hodnota proměnné `covid` bude navýšena o výsledek funkce pro výpočet nakažení na základě **agresivity** viru, **zdraví** agenta a aktuální hodnotě / úrovní **nakažení** nalezeného agenta.
    * Agresivitu viru je možné nastavit dle vlastních potřeb (_pro testovací účely_).
        * Používá se pro nastavení (/ _přizpůsobení_), _jak rychle_ bude agent nakažen virem.
    * Slouží k tomu posuvník `virus-aggression`.
* **Následně se celý proces opakuje**, tedy buď agent jde nebo nejde domů (_podle toho jak je nastaveno pro simulaci a zdali je agent nakažen_), pokud se pohybuje po modelu, hledají se infikovaní agenti v okolí, přepočítá se hodnota `covid`, pokud se vyléčí (hodnota v proměnné `covid` klesne na **nulu**), agent bude po omezenou dobu **imunní** a následně může být znovu nakažen.



### Léčba

* Agent může být vyléčen tak, že půjde domů, kde se nemůže od nikoho nakazit a tam zůstane po dostatečně dlouhou dobu (_počet tiků_) než mu klesne hodnota covid na **nulu**. To závisí na tom, jak je aktuální hodnota zmíněné proměnné vysoká.
    * _Po vyléčení bude agent po omezenou dobu imunní._
* Je tu šance, že agent zemře dříve, než se vyléčí, to může nastat tak, že agent dosáhne maximálního možného věku nebo mu selže zdraví. Viz kapitola **Zdraví**. 



### Zdraví

* Agent má atribut pro zdraví (`health`).
    * Hodnota může být v intervalu **<0 ; 100>**.
    * Určuje aktuální **míru zdraví** příslušného agenta.
    * Pokud hodnota klesne na **nulu**, agent **zemře**.
* Hodnota proměnné je **snižována dle** určité **funkce** zahrnující pravděpodobnost úmrtí (_údaje jsou získány z Amerických zdrojů, pravděpodobnosti jsou určeny na základě pohlaví, věku a zdali agent má nebo nemá covid_) a míře nakažení (_hodnota v `covid` proměnné_).
    * Hodnota zdraví je **snížena jednou za rok**.
        * To slouží pro simulaci snížení imunity agenta (_stárnutí_).
    * Dále je hodnota **snížena při každém ticku**, kdy má agent hodnotu `covid > 50`.
        * Slouží pro simulaci zhoršení zdraví, když má agent těžký průběh nemoci způsobenou virem.



## Pohlaví

* Každý agent má pohlaví (`sex`).
    * _V modelu je pohlaví reprezentováno dvěma hodnotami, **0** značí **muže**, **1 ženu**._
* Pro nastavení / přiřazení pohlaví agentům jsou k dispozici následující možnosti.
    * `random`
        * Každému agentovi bude vygenerováno náhodné pohlaví.
    * `men`
        * Všichni agenti budou mít mužské pohlaví.
    * `women`
        * Všichni agenti budou mít ženské pohlaví.
    * `number-of-men`
        * Pomocí posuvníku `number-of-agents-for-gender` bude nastaven počet agentů **mužského** pohlaví, všichni **ostatní** agenti budou **ženského** pohlaví.
    * `number-of-women`
        * Pomocí posuvníku `number-of-agents-for-gender` bude nastaven počet agentů **ženského** pohlaví, všichni **ostatní** agenti budou **mužského** pohlavi.
    * `random-number-of-men`
        * Pomocí posuvníku `max-random-number-of-agents-for-gender` bude nastaven maximální počet agentů, který může být vygenerován.
        * Náhodně vygenerované číslo v intervalu **<1 ; `max-random-number-of-agents-for-gender`>** bude značit počet agentů, kteří budou **mužského** pohlaví. Všichni **ostatní** agenti budou **ženského** pohlaví.
    * `random-number-of-women`
        * Pomocí posuvníku `max-random-number-of-agents-for-gender` bude nastaven maximální počet agentů, který může být vygenerován.
        * Náhodně vygenerované číslo v intervalu **<1 ; `max-random-number-of-agents-for-gender`>** bude značit počet agentů, kteří budou **ženského** pohlaví. Všichni **ostatní** agenti budou **mužského** pohlaví.
* Hodnota posuvníku `number-of-agents-for-gender` a `max-random-number-of-agents-for-gender` musí být nastavena na hodnotnu **menší**, než je nastaven **celkový počet agentů** v modelu.



## Rouška

* Každý agent má (/ _nosí_) nebo nemá roušku. V modelu je rouška reprezentována atributem `veil?` (`boolean`).
* Agentovi je rouška přiřazena podle níže uvedených možností (/ nastavení).
    * _V modelu je agentům nejprve přiřazeno pohlaví, následně rouška._
    * `random`
        * Každému agentovi je rouška přiřazena **náhodně**.
    * `all`
        * **Všichni** agenti mají roušku (_nehledně na pohlaví_).
    * `no-one`
        * **Žádný** agent nemá roušku.
    * `men`
        * Agent má roušku pouze tehdy, je-li **mužského** pohlaví.
    * `women`
        * Agent má roušku pouze tehdy, je-li **ženského** pohlaví.



## Pohyb agentů

* Po spuštění modelu se agenti volně pohybují po prostředí.
* Každý agent může vstoupit pouze do svého domu (_dům stejné barvy jako je barva příslušného agenta_).
* Každý následující krok agenta je vygenerován náhodně.
    * Vždy se posune o jedno `políčko` vpřed.
    * Směr je vygenerován **náhodně** mezi **levou** a **pravou** stranou s **natočením** v intervalu **0 - 10** stupňů na levou nebo pravou stranu.
        * Tím je zaručen _přiměřený_ pohyb agentů po prostředí. Tedy nepohybují se vždy v jednom směru nebo směr není vygenerován náhodně v 8 směrech okolo agenta.



### Překážky

* Při pohybu agenta jsou vyhodnocovány situace, kam agent může a nemůže _vstoupit_.
* Při určení každého následujícího pohybu je vyhodnoceno, zdali se **ve směru agenta o 3 políčka ve 180 stupňovém rozmezí** nachází překážka.
    * Pokud **ano**, agent se otočí **doprava o 90 stupňů** a posune se o krok _vpřed_.
        * Tím je zajištěno, že se agent překážce vyhne.
    * Pokud **ne**, agent se v příslušném směru posune dle výše popsaného vygenerovaného směru.
* **Překážka** je pro agenta **vše**, co **není** obarveno barvou **pozadí** nebo nebo **barvou jeho domu** (_do kterého patří_).



#### Nastavení

* K dispozici jsou následující možnosti nastavení / vygenerování překážek.
* Zvlášť pro svislé a vodorovné překážky je možné nastavit individuální nebo shodnou barvu.

* **Typy**
    * `none`
        * Nebudou vygenerovány žádné překážky.
    * `horizontal-lines`
        * Budou vygenerovány **dvě** přerušované vodorovné _čáry_, které budou obarveny zvolenou barvou.
        * Tyto _čáry_ budou reprezentovat překážku (/ _překážky_) skrze kterou agent nemůže projít.
    * `vertical-lines`
        * Budou vygenerovány celkem **4** přerušované svislé čáry. **Dvě** v 1/4 modelu a druhé **dvě** v 3/4 modelu.
        * Tyto _čáry_ budou reprezentovat překážku (/ _překážky_) skrze kterou agent nemůže projít. 
    * `horizontal-and-vertical-lines`
        * V modelu budou vygenerovány výše popsané **vodorovné** a **svislé** _čáry_ reprezentující překážky.



# Proces (procedura `go`)

Znázornění postupu (/ _kroků_) procedury `go`. Jedno provedení procedury (_jeden **tick**_) reprezentuje simulaci uplynutí **jedné hodiny**.

* V rámci jednoho provedení procedury dojde k iteraci všech agentů aktuálně existujících v modelu. V každé iteraci se pro každého agenta provedou následující kroky.

* **Zjištění barvy domu, do kterého patří příslušný agent.**
    * Je potřeba níže pro zjištění překážky a zdali není agent doma při zjištění, zdali se může nakazit virem.
* **Vyhledání a vyhnutí se překážám.**
    * V rozmezí (_vzdálenosti_) **3** políček (_patch-ů_) v **360** stupňovém okolí agenta budou vyhledány veškeré objekty, které mají jinou barvu než barvu agentova domu nebo barvu pozadí prostředí.
    * Pokud bude libovolný objekt, který splňuje tyto podmínky nalezen (objekt nemá žádnou z barev), jedná se o překážku, které se agent vyhne tak, že se pootočí o **90** stupňů **doprava**.
        * _Tato část by šla upravit, že by se například vygeneroval náhodný směr pootočení agenta nebo nastavila specifická strategie._ 
    * Pokud nebude překážka nalezena, bude vygenerován náhodný stupeň natočení agenta směrem doleva nebo doprava o **0 - 10** stupňů.
    * Následně se agent posune výše nastaveným směrem o jedno políčko vpřed.
* **Určení podmínky, zdali má jít agent domů.**
    * V této části bude zjištěno, zdali je agent nakažen virem (`covid > 0`). Pokud ano a je tak nastaveno (`go-home-when-agent-is-infected? = true`), agent bude natočen takovým směrem, aby šel domů, kde se bude léčit. Dle potřeby se vyhne překážce.
* **Kontrola a provedení kroků pro určení vypršení imunity.**
    * Pokud agent není nakažen (**covid = 0**) ale má imunitu, kdy nemůže být nakažen (nedávno byl vyléčen / zbaven viru), bude dle potřeby navýšena hodnota pro určení vypršení imunity.
    * Pokud hodnota dosáhla stavu, kdy imunita vyprchá, budou nastaveny hodnoty k tomu, aby agent byl zbaven imunity a mohl být opět infikován virem.
* **Snížení hodnoty v proměnné `covid` o 0.5 .**
    * Slouží pro simulaci zlepšování zdravotního stavu v rámci léčení viru.
* **Test nakažení.**
    * Podle aktuální pozice iterovaného agenta bude zjištěno, zdali není doma.
    * Dále, že nemá imunitu a může být infikován virem.
    * Pokud jsou podmínky splněny, podle toho, zdali má nebo nemá roušku budou vyhledány agenti v okolí, kteří nejsou doma a mají úroveň `covid` větší než **25**.
    * Pro každého nalezeného agenta v okolí bude vypočtena funkce pro výpočet nakažení.
        * Výsledek funkce bude přičten k aktuální hodnotě `covid` iterovaného agenta. Bude pohlídáno, aby nová hodnota covid **nepřesáhla** mximální hodnotu **100**.
* **Zjištění nastavení imunity.**
    * Pokud byl agent nakažen virem a po přepočtení funkce nakažení a snižování hodnoty `covid` v rámci léčení již nakažen není, bude mu nastavena imunita.
    * Simulace případu, kdy byl agent vyléčen / zbaven viru a bude mu po určitou dobu imunní (`immunity-after-healing`).
* **Kontrola a dle potřeby snížení zdraví.**
    * Agent má vlastnost zdraví (`health`).
    * Zdraví je sníženo každým rokem.
    * Pokud aktuální počet uplynutých tiků odpovídá jednomu roku, bude provedena funkce pro výpočet snížení zdraví.
    * Stejná funkce pro snížení zdraví bude provedena i pokud aktuální hodnota `covid` příslušného agenta je **vyšší než 50**.
        * Simulace těžkého průběhu nemoci.
* **Navýšení věku.**
    * Pokud aktuální počet uplynutých tiků v modelu dosáhl jednoho roku, agentovi bude navýšeno stáří o jeden rok.
    * Agenti mají v podstatě navýšení roku ve stejný čas, protože se plynutí tiků (/ _času_) počítá se spuštěním simulace pro všechny agenty stejně.
        * V rámci vylepšení této části by bylo vhodné přidat nějakou strategii či náhodné generování _narozenin_ agenta.
        * Ale jednalo by se pouze o nastavení atributu `age-tick` na například náhodnou hodnotu při spuštění simulace. Díky tomu by stáří agentů bylo různé.
* **Kontrola úmrtí.**
    * Pokud agent dosáhl věku **100** nebo hodnota zdraví klesla na **nulu**, agent zemře.
