breed [people person]

turtles-own [
  id
  age ; Current age of the agent
  age-tick ; Variable to (temporarily) store the number of ticks representing the deduction until the year increase, ie with each tick (as 1 hour) this value of the variable will increase by 1 until the value reaches 8760 (= 1 year in ticks), the age of the agent will be increased by 1 (year) and this variable will be reset
  home-color ; Color of the house to which the agent belongs (his home)
  health ; Agent health level <0; 100>. If the value drops to zero, the agent dies.
  covid ; Agent infection level <0; 100>, up to 25 (inclusive) it is a 'mild' / 'incubation' level where the agent is not infectious (cannot infect other agents in the vicinity)
  sex ; Agent gender, 0 = male, 1 = female
  veil? ; true if the agent has a veil, otherwise false
  can-be-infected? ; It represents the 'immunity' of an agent, i.e. whether an agent can be infected or not, for example when an agent has been cured of a virus, it cannot be infected for a certain period of time (tick), this is determined by this value, true -> the agent can be infected with a virus, so it has no immunity, false -> the agent has immunity, it cannot be infected with the virus for some time
  can-be-infected-tick ; The number of ticks, how long the agent is cured of the virus, when it reaches a certain value, the can-be-infected attribute will be set to true, so the agent will no longer be immune to the virus
]

globals [
  year ; Number of ticks in one year
  dead-agents ; The number of agents who died
]

to setup
  ca

  ; Setting the (default) values​of global variables
  set year 8760
  set dead-agents 0

  setup-world

  reset-ticks
end

; Creating the world (backgrounds, houses, agents / people, etc.)
to setup-world
  if not are-all-colors-unique [
    stop
  ]

  setup-background-color
  setup-houses
  setup-obstacles
  setup-people
end

; Check that no duplicate colors are set, for example, whether all home colors are unique, do not coincide with the background, etc.
; Returns true if all desired colors are unique, otherwise false (at least one desired color is not unique)
to-report are-all-colors-unique
  ; Check that the colors of the houses are unique
  if house-color-1 = house-color-2 or house-color-1 = house-color-3 or house-color-1 = house-color-4 or house-color-1 = house-color-5 or house-color-1 = house-color-6 [
    user-message (word "The color of house 1 is not unique.")
    report false
  ]
  if house-color-2 = house-color-3 or house-color-2 = house-color-4 or house-color-2 = house-color-5 or house-color-2 = house-color-6 [
    user-message (word "The color of house 2 is not unique.")
    report false
  ]
  if house-color-3 = house-color-4 or house-color-3 = house-color-5 or house-color-3 = house-color-6 [
    user-message (word "The color of house 3 is not unique.")
    report false
  ]
  if house-color-4 = house-color-5 or house-color-4 = house-color-6 [
    user-message (word "The color of house 4 is not unique.")
    report false
  ]
  if house-color-5 = house-color-6 [
    user-message (word "The color of house 5 is not unique.")
    report false
  ]

  ; Check that the color of the obstacles does not match the color of the house
  if horizontal-obstacle-color = house-color-1 or horizontal-obstacle-color = house-color-2 or horizontal-obstacle-color = house-color-3 or horizontal-obstacle-color = house-color-4 or horizontal-obstacle-color = house-color-5 or horizontal-obstacle-color = house-color-6 [
    user-message (word "The color of the horizontal obstacle is the same as the color of the house.")
    report false
  ]
  if vertical-obstacle-color = house-color-1 or vertical-obstacle-color = house-color-2 or vertical-obstacle-color = house-color-3 or vertical-obstacle-color = house-color-4 or vertical-obstacle-color = house-color-5 or vertical-obstacle-color = house-color-6 [
    user-message (word "The color of the vertical obstacle is the same as the color of the house.")
    report false
  ]

  ; Check that the background color does not match the color of the house
  if background-color = house-color-1 or background-color = house-color-2 or background-color = house-color-3 or background-color = house-color-4 or background-color = house-color-5 or background-color = house-color-5 [
    user-message (word "The background color is the same as the house color.")
    report false
  ]

  ; Check that the background color does not match the obstacle color
  if background-color = horizontal-obstacle-color or background-color = vertical-obstacle-color [
    user-message (word "The background color is the same as the obstacle color.")
    report false
  ]

  report true
end

; Colorize the background color (/ environment) with the selected / set color
to setup-background-color
  ask patches [
    set pcolor background-color
  ]
end

to setup-houses
  let x 0
  let y 0

  while [y < world-height] [
    while [x < world-width] [
      ; Upper 3 houses
      if x < 10 and y > 50 [setup-house x y house-color-1]
      if x >= 50 and x < 60 and y > 50 [setup-house x y house-color-2]
      if x > 100 and y > 50 [setup-house x y house-color-3]
      ; Lower 3 houses
      if x < 10 and y < 10 [setup-house x y house-color-4]
      if x >= 50 and x < 60 and y < 10 [setup-house x y house-color-5]
      if x > 100 and y < 10 [setup-house x y house-color-6]

      set x x + 1
    ]
    set x 0
    set y y + 1
  ]
end

to setup-house [x y house-color]
  ask patches with [pxcor = x and pycor = y] [
    set pcolor house-color
  ]
end

; Create obstacles according to the selected option in the model
to setup-obstacles
  (
    ifelse obstacle = "horizontal-lines" [
      setup-horizontal-obstacles
    ]
    obstacle = "vertical-lines" [
      setup-vertical-obstacles

    ]
    obstacle = "horizontal-and-vertical-lines" [
      setup-horizontal-obstacles
      setup-vertical-obstacles
    ]
  )
end

; Create (/ draw) horizontal 'obstacles / lines'
to setup-horizontal-obstacles
  let upper-ycor 25
  let lower-ycor 35

  let x 0
  let upper-obstacle true

  while [x < world-width] [
    ; Horizontal and vertical line (/ obstacle) alternates every 10 patches
    if x mod 10 = 0 and x mod (world-width - 1) != 0 [
      set upper-obstacle not upper-obstacle
    ]

    ifelse  upper-obstacle [
      setup-obstacle x upper-ycor horizontal-obstacle-color
    ]
    [
      setup-obstacle x lower-ycor horizontal-obstacle-color
    ]

    set x x + 1
  ]
end

; Create (/ draw) vertical 'obstacles / lines'
to setup-vertical-obstacles
  let draw-line true

  ; Draw a dashed vertical line for the list of x-coordinates
  foreach [24 34 76 86] [x ->
    let y 0
    while [y < world-height] [
      ; Every 10 patches either draw / do not draw a line representing the obstacle (otherwise there will be a blank space)
      if y mod 10 = 0 and y mod (world-height - 1) != 0 [
        set draw-line not draw-line
      ]

      if draw-line [
        setup-obstacle x y vertical-obstacle-color
      ]

      set y y + 1
    ]
  ]
end

; Create (/ Draw) an obstacle at the specified coordinates
; x and y are the coordinates on which the obstacle will be drawn (colored patch)
; obstacle-color is the color representing the obstacle, ie the color that the patch will be colored at the specified coordinates
to setup-obstacle [x y obstacle-color]
  ask patches with [pxcor = x and pycor = y] [
    set pcolor obstacle-color
  ]
end

; Create people (/ agents) in prepared positions
to setup-people
  ; Creating (/ declaring) a set number of people (total)
  create-people people-of-house-1 + people-of-house-2 + people-of-house-3 + people-of-house-4 + people-of-house-5 + people-of-house-6

  ; Id with which the individual agents will be accessed
  let person-id 0

  let upper-ycor 48
  let lower-ycor 12

  ; Add / create a set number of 'people' to each house
  setup-person-to-house person-id people-of-house-1 house-color-1 4 upper-ycor
  set person-id person-id + people-of-house-1

  setup-person-to-house person-id people-of-house-2 house-color-2 54 upper-ycor
  set person-id person-id + people-of-house-2

  setup-person-to-house person-id people-of-house-3 house-color-3 106 upper-ycor
  set person-id person-id + people-of-house-3

  setup-person-to-house person-id people-of-house-4 house-color-4 4 lower-ycor
  set person-id person-id + people-of-house-4

  setup-person-to-house person-id people-of-house-5 house-color-5 54 lower-ycor
  set person-id person-id + people-of-house-5

  setup-person-to-house person-id people-of-house-6 house-color-6 106 lower-ycor

  ; If set, a random number of agents will be assigned a male or female gender
  generate-random-gender

  ; Setting (/ assigning) drapes to agents based on specified parameters
  setup-veils

  ; Setting up infected agents
  setup-infected-agent
end

; Creating people / agents who will belong to a specific house
; person-id - id of the agent, which will be used to set properties for one specific agent, will be incremented via an auxiliary variable, because there are no references
; people-of-house - the number of agents to be created for the house in question
; house-color - house color
; x, y are the default coordinates of the agent (these are the coordinates in front of the agent house, they will only be used if no random agent location is set)
to setup-person-to-house [person-id people-of-house house-color x y]
  let tmp-person-id person-id
  let index 0

  while [index < people-of-house] [
    setup-person tmp-person-id house-color x y
    set tmp-person-id tmp-person-id + 1
    set index index + 1
  ]
end

; Setting the default properties of the agent with id in the procedure parameter, the other parameters are the color of the agent and its default coordinates (they will be used unless a random agent location is set)
to setup-person [id-param house-color x y]
  ask turtles with [who = id-param] [
    set id id-param
    set size 2
    set shape "person"
    set color house-color
    set home-color house-color

    ; Age setting
    set age-tick 0
    set age get-starting-age

    set health 100

    ; Covid settings (infection)
    set covid 0

    ; The agent is not infected, but neither does he have immunity, so he can become infected
    set can-be-infected? true
    set can-be-infected-tick 0

    ; Agent gender settings
    set sex get-gender

    ; Setting the default coordinates of the agent (in front of his house)
    let x-cor x
    let y-cor y
    ; If a random agent location is set, random coordinates will be generated until a location is found that is either in his house or outside the obstacle
    if generate-random-people-position? [
      set x-cor random world-width
      set y-cor random world-height
      while [is-obstacle-in-coordinates x-cor y-cor house-color] [
        set x-cor random world-width
        set y-cor random world-height
      ]
    ]
    ; Setting the coordinates of the agent where it will be located when starting the model / simulation
    setxy x-cor y-cor
  ]
end

; Find out if there is any obstacle at the specified coordinates (on which the agent cannot be placed)
; An obstacle is for an agent anything that is not the background color or the color of his house
to-report is-obstacle-in-coordinates [x-cor y-cor house-color]
  let is-obstacle-in-coordinates? false

  ask patches with [pxcor = x-cor - 1 and pycor = y-cor] [
    if is-obstacle pcolor house-color [
      set is-obstacle-in-coordinates? true
    ]
  ]

  ask patches with [pxcor = x-cor and pycor = y-cor] [
    if is-obstacle pcolor house-color [
      set is-obstacle-in-coordinates? true
    ]
  ]

  ask patches with [pxcor = x-cor + 1 and pycor = y-cor ] [
    if is-obstacle pcolor house-color [
      set is-obstacle-in-coordinates? true
    ]
  ]

  ask patches with [pxcor = x-cor and pycor = y-cor - 1] [
    if is-obstacle pcolor house-color [
      set is-obstacle-in-coordinates? true
    ]
  ]

  ask patches with [pxcor = x-cor and pycor = y-cor ] [
    if is-obstacle pcolor house-color [
      set is-obstacle-in-coordinates? true
    ]
  ]

  ask patches with [pxcor = x-cor and pycor = y-cor + 1] [
    if is-obstacle pcolor house-color [
      set is-obstacle-in-coordinates? true
    ]
  ]

  report is-obstacle-in-coordinates?
end

; Determining whether the patch-color field is an obstacle for the agent, returns true, if so, otherwise it returns false
; The procedure finds out whether the color of the patch is not the color of the obstacle, so it can be the color of the background or his house, everything else is obstacle
; house-color - the color of the house to which the agent relates, what is the permitted location, is therefore not an obstacle
to-report is-obstacle [patch-color house-color]
  report patch-color = horizontal-obstacle-color or patch-color = vertical-obstacle-color or (patch-color != house-color and patch-color != background-color)
end

; Obtaining a starting age for the appropriate agent
; If the age is set to be generated randomly, a random number will be generated in the specified interval (including endpoints)
; If the age is not to be generated, the set number (/ age) will be returned
to-report get-starting-age
  ifelse generate-age-randomly? [
    ; Generate a random number in the specified interval (including the extreme), ie both endpoints
    report random (maximum-age - minimum-age + 1) + minimum-age
  ]
  [
    report starting-age
  ]
end

; Obtaining gender (for the relevant agent) according to the requirements set in the model
to-report get-gender
  (
    ifelse gender = "random" [
      ; The agent gender will be randomly generated
      report random 2
    ]
    gender = "men" or gender = "number-of-women" or gender = "random-number-of-women" [
      ; men - male sex
      ; number-of-women - a specific number of women will be set, so first all agents will be set as men and then the required number of agents will be set to female gender (in 'generate-random-gender')
      ; random-number-of-women - a random number of women will be set / generated, therefore first all agents will be set as men and then the female gender will be set to the generated number of agents (in 'generate-random-gender')
      report man
    ]
    gender = "women" or gender = "number-of-men" or gender = "random-number-of-men" [
      report woman
    ]
  )
end

; Generating (/ assigning) either the desired or a random number of agents of the desired gender
to generate-random-gender
  ; Array for storing agent ids that have been assigned the required gender
  ; It is need to store them here so that no agent is counted more than once
  let used-agent-ids []
  let index 0

  ; Setting the required gender to the set number of agents
  if gender = "number-of-men" or gender = "number-of-women" [
    ifelse number-of-agents-for-gender > count turtles [
      let message "The number of agents to be male is greater than the total number of agents in the model. All agents will be female."
      if gender = "number-of-women" [
        set message "The number of agents to be female is greater than the total number of agents in the model. All agents will be male."
      ]
      user-message (word message)
    ]
    [
      output-print (word number-of-agents-for-gender " agents will be set to the required gender.")

      ; Assign the required gender to random agents
      let tmp-gender man
      if  gender = "number-of-women" [
        set tmp-gender woman
      ]
      generate-gender tmp-gender number-of-agents-for-gender
    ]
  ]

  ; Setting the required gender to a randomly generated number of agents (with the maximum number set)
  if gender = "random-number-of-men" or gender = "random-number-of-women" [
    ifelse max-random-number-of-agents-for-gender > count turtles [
      let message "The random number of agents to be male is greater than the total number of agents in the model. All agents will be female."
      if gender = "random-number-of-women" [
        set message "The random number of agents to be female is greater than the total number of agents in the model. All agents will be male."
      ]
      user-message (word message)
    ]
    [
      ; Generating a random number of agents to be assigned the required gender
      let tmp-random-number-of-agents random max-random-number-of-agents-for-gender
      output-print (word tmp-random-number-of-agents " agents will be set to the required gender.")

      ; Assign the required gender to random agents
      let tmp-gender man
      if  gender = "random-number-of-women" [
        set tmp-gender woman
      ]
      generate-gender tmp-gender tmp-random-number-of-agents
    ]
  ]
end

; Generating (/ assigning) number-of-agents agents gender in gender-param
; gender-param - the gender to be assigned to agents
; number-of-agents - the number of agents for which the gender is to be set in the previous parameter
to generate-gender [gender-param number-of-agents]
  ; Array for storing agent ids that have been assigned the required gender
  ; It is need to store them here so that no agent is counted more than once
  let used-agent-ids []
  let index 0

  while [index < number-of-agents] [
    ; Random generation of an agent id to which the required gender will be assigned
    let random-agent-id random count turtles
    while [member? random-agent-id used-agent-ids] [
      set random-agent-id random count turtles
    ]

    ; Insert the generated agent id at the end of the list so that the agent with the appropriate id is not counted more than once as the agent to which the appropriate gender has been set
    set used-agent-ids lput random-agent-id used-agent-ids

    ; Setting up an agent with a generated id of the desired gender
    ask turtles with [who = random-agent-id] [
      set sex gender-param
    ]

    set index index + 1
  ]
end

; Generation (/ assignment) of all veil agents based on user-set parameters and agent gender
to setup-veils
  ask turtles [
    set veil? get-veil sex
  ]
end

; Obtaining information about whether the agent will have a veil or not
; sex-param is an index of 0 or 1, depending on whether the agent is male or female
to-report get-veil [sex-param]
  (
    ifelse veil = "random" [
      report one-of [true false]
    ]
    veil = "all" [
      report true
    ]
    veil = "no-one" [
      report false
    ]
    veil = "men" [
      report is-man sex-param
    ]
    veil = "women" [
      report not is-man sex-param
    ]
  )
end

; Obtaining an index representing the male sex (substitute for the enumeration value)
to-report man
  report 0
end

; Obtaining an index representing the female sex (substitute for the enumeration value)
to-report woman
  report 1
end

; Find out if there is a male agent index in the procedure parameter
; report true if it is a male index, otherwise false (female gender)
to-report is-man [sex-param]
  report sex-param = man
end

; Set / Generate agent infection
to setup-infected-agent
  ; The total number of agents to be infected
  let number-of-infected starting-number-of-infected
  if random-number-of-infected? [
    set number-of-infected random maximum-number-of-infected + 1
  ]

  output-print (word number-of-infected " agents will be infected.")

  ; If more infected agents are specified than the total number of agents in the model at the default (first) model creation, no agent will be set as infected
  ifelse number-of-infected > count turtles [
    user-message (word "The set / generated number of infected agents is higher than their number in the model, no agent will be infected.")
  ]
  [
    ; A list for storing agent ids that will be set to be infected
    ; In order not to set up an agent with one id for example 5 times that he is infected
    let used-agent-ids []
    let index 0

    while [index < number-of-infected] [
      ; Randomly generate an agent id to be infected
      let random-agent-id random count turtles
      while [member? random-agent-id used-agent-ids] [
        set random-agent-id random count turtles
      ]

      ; Insert the generated agent id at the end of the list so that this agent id is not reused as the agent to be set to be infected
      set used-agent-ids lput random-agent-id used-agent-ids

      ; Set the attribute of the agent with the generated id that is infected
      ask turtles with [who = random-agent-id] [
        set covid get-level-of-virus-infection
        ; The agent is infected with a virus, so it will not even have immunity to the virus
        set can-be-infected? true
        set can-be-infected-tick 0
      ]

      set index index + 1
    ]
  ]
end

; Obtaining the default virus infection rate (ie 'how much' the agent will be infected at the beginning of the simulation)
to-report get-level-of-virus-infection
  if randomly-generate-infection-rates? [
    report random 100 + 1
  ]
  report initial-infectivity-rate
end

; Setting all default values
to default-values
  ; Set default colors
  set background-color black
  set house-color-1 red
  set house-color-2 green
  set house-color-3 orange
  set house-color-4 brown
  set house-color-5 yellow
  set house-color-6 blue

  set horizontal-obstacle-color magenta
  set vertical-obstacle-color magenta

  ; Set the default number of people
  let default-number-of-people 5
  set people-of-house-1 default-number-of-people
  set people-of-house-2 default-number-of-people
  set people-of-house-3 default-number-of-people
  set people-of-house-4 default-number-of-people
  set people-of-house-5 default-number-of-people
  set people-of-house-6 default-number-of-people

  set generate-random-people-position? true

  ; Set default obstacle
  set obstacle "horizontal-lines"

  ; Default values related to the age setting
  set generate-age-randomly? true
  set minimum-age 25
  set maximum-age 50
  set starting-age 25

  ; Default values for the number of infected people
  set go-home-when-agent-is-infected? true
  set random-number-of-infected? true
  set maximum-number-of-infected 30
  set starting-number-of-infected 50
  set randomly-generate-infection-rates? true
  set initial-infectivity-rate 50
  set immunity-after-healing 168
  set virus-aggression 5

  ; Set default values related to the gender of agents
  set gender "random"
  set number-of-agents-for-gender 50
  set max-random-number-of-agents-for-gender 50

  ; Setting the default value for drapes
  set veil "random"

  ; Setting the default distance for the possibility of virus infection when the agent wears / does not wear a veil
  set distance-for-infection-with-veil 1
  set distance-for-infection-without-veil 3
end

; Generate random numbers of people in individual houses
to generate-random-people-of-houses
  set people-of-house-1 random 101
  set people-of-house-2 random 101
  set people-of-house-3 random 101
  set people-of-house-4 random 101
  set people-of-house-5 random 101
  set people-of-house-6 random 101
end

; Performing one cycle (/ tick) of people's 'lives'
to go
  ; For each agent, find out if he has an obstacle in his direction, if so, turn 90 degrees to the right to avoid it
  ask turtles [
    ; Color of the agent's house
    let turtle-home-color home-color

    let obstacle-found false
    ask patches in-cone 3 180 [
      ; Determine if there is an obstacle in the circuit of 3 patches within 180 degrees in front of the agent
      ; An obstacle is anything that is not a patch with the color of the agent's house, the background color or horizontal or vertical obstacle
      if pcolor != turtle-home-color and pcolor != background-color [
        set obstacle-found true
      ]
    ]

    ; Generating the direction of the next agent movement
    let tmp-lt random 10
    let tmp-rt random 10

    ; If an obstacle is found, the agent turns 90 degrees to the right to avoid it
    if obstacle-found [
      set tmp-rt 90
    ]

    ; Agent movement according to the above generated / set values
    lt tmp-lt
    rt tmp-rt
    fd 1

    ; Here the condition for covid > 0, in which case the agent is infected with a virus and should go home for treatment, ie he should be quarantined, but this does not mean that he has a 'severe course of the disease' and is infectious (ie simulation, that the agent was infected, for example, yesterday)
    if covid > 0 [
      ; The agent is infected, in which case it will be determined whether it is set for the agent to go home or not and whether to avoid an obstacle (if it stands in his way)
      ; When agent encounters an obstacle, he prefers to avoid it and then continues in the direction of his home
      if go-home-when-agent-is-infected? and not obstacle-found [
        face one-of patches with [pcolor = turtle-home-color]
      ]
    ]

    ; Check that the agent's immunity has not yet expired so that he can become infected with the virus again
    ; The agent has no covid at all (has been cured, has immunity), and cannot be infected (has immunity), so the virus has just been cleared (cured), so the time will be 'counted' for how long it will be immune and possibly immunity will be removed and the agent will will be able to be infected again
    if covid = 0 and not can-be-infected? [
      ; Here the agent does not have a covid but has immunity, so he cannot be infected for some time because he has (recently) had it
      ; Increment the time the agent does not have a virus
      set can-be-infected-tick can-be-infected-tick + 1

      ; If the agent has reached the time for which he can be immune to the virus, it is set that he can be re-infected because his immunity has ended
      if can-be-infected-tick >= immunity-after-healing [
        set can-be-infected? true
        set can-be-infected-tick 0
      ]
    ]

    ; When an agent has at least a minimum number of viruses, it will decrease with each tick until it is zero (simulation of improving the health of the agent)
    if covid > 0 [
      set covid covid - 0.5
      ; For the case when a decimal number is obtained, from which 1 will be subtracted, for example 0.5 - 1
      if covid < 0 [
        set covid 0
      ]
    ]

    ; Determining whether an agent can / will be infected or not
    let turtle-id id
    let turtle-health health
    let turtle-covid covid
    let turtle-has-covid? covid > 0
    let turtle-has-veil? veil?
    let tmp-x xcor
    let tmp-y ycor

    ; Finding out if the agent is in his house, if so, he will not be infected
    let is-tested-agent-home? is-agent-home tmp-x tmp-y turtle-home-color

    ; Here, it is tested whether the agent is in the presence of another agent that is infected with the virus, but this is only tested if the agent is not at home and is not immune to the virus (it has not recently been cured)
    if not is-tested-agent-home? and can-be-infected? [
      ; Determining the distance agent can become infected
      ; If the agent has a veil, the distance for the agent to become infected must be shorter (the agent must be closer to the infected agent) than if the agent did not have a veil
      let distance-for-infection distance-for-infection-with-veil
      if not turtle-has-veil? [
        set distance-for-infection distance-for-infection-without-veil
      ]

      ; Search for all agents that are at a set distance (according to the veil) from the current iterated agent
      ask turtles in-cone distance-for-infection 360 with [id != turtle-id and covid > 25] [
        ; Position where the found agent is located near the tested agent
        let close-turtle-x-cor xcor
        let close-turtle-y-cor ycor
        let close-turtle-home-color home-color
        ; Determining if an agent is an agent at home, if so, will not test for infection
        let is-close-agent-home? is-agent-home close-turtle-x-cor close-turtle-y-cor close-turtle-home-color

        ; If neither agent is at home (moving around the environment / model), the tested agent may be infected (increased value of the covid variable) from an agent that is nearby
        if not is-tested-agent-home? and not is-close-agent-home? [
          if turtle-covid < 100 [ ; when the covid is at maximum, raising makes no sense
            let infection-calculation turtle-covid + ((virus-aggression / 100) * turtle-health * (covid / 100))
            set turtle-covid precision infection-calculation 3
          ]
        ]
      ]
    ]

    ; Setting the newly calculated value / level of virus infection (if the value has changed)
    ifelse turtle-covid > 100 [
      set covid 100
    ]
    [
      set covid turtle-covid
    ]

    ; The agent has just been cured (/ got rid of) of the virus, so he will have immunity for a certain period of time, ie he will not be able to become infected for some time
    ; It needs to be tested to see if he has (had) a covid, otherwise any uninfected agent would gain immunity
    if turtle-has-covid? and round covid = 0 [
      set can-be-infected? false
      set can-be-infected-tick 0
    ]

    ; Decreased health of the agent every year (aging / reduced immunity) or if the agent is infected with a virus (weakened immunity)
    if ticks mod year = 0 or covid > 50 [
      ; First, health is calculated including covid virus infection, but if the agent is not infected at all, the result will be zero, in which case the health will decrease by a constant value (the strength of health / immunity decreases with age)
      let health-calculation precision (health - get-probability-of-death sex covid age * (covid / 100) * age) 3
      ifelse health-calculation = 0 [
        set health health - 1
      ]
      [
        set health health-calculation
      ]
    ]

    ; Increasing the value for counting the age of an agent, if the agent 'ages' by one year, his age will be increased
    set age-tick age-tick + 1
    if age-tick >= year [
      set age-tick 0
      set age age + 1
    ]

    ; Checking the age of the agent, if he reaches the maximum age, he will die
    ; If the agent's health (old age or immunity) expires, he will also die
    if age >= 100 or health <= 0 [
      set dead-agents dead-agents + 1
      die
    ]
  ]

  tick
end

; Find out if the agent at the specified coordinates is at home
; If the color of the field (/ patch) on which the agent is located is the same as the color of the agent's house (turtle-home-color), the agent is at home, otherwise not
; x-cor, y-cor - coordinates (/ position) where the agent is currently located
; turtle-home-color - color of the agent's house
to-report is-agent-home [x-cor y-cor turtle-home-color]
  let agent-is-home? false

  ask patches with [pxcor = round x-cor and pycor = round y-cor] [
    if pcolor = turtle-home-color [
      set agent-is-home? true
    ]
  ]

  report agent-is-home?
end

; Obtaining the probability of agent death in percent based on input parameters
; Percent was obtained from a source regarding information on deaths from covid-19 virus infection
to-report get-probability-of-death [sex-param covid-param age-param]
  ifelse is-man sex-param [
    ifelse covid-param > 0 [
      if age-param > 1 and age-param <= 24 [
        report 0.00052
      ]
      if age-param <= 44 [
        report 0.00872
      ]
      if age-param <= 64 [
        report 0.05947
      ]
      report 0.33864
    ]
    [
      if age-param > 1 and age-param <= 24 [
        report 0.00209
      ]
      if age-param <= 44 [
        report 0.00867
      ]
      if age-param <= 64 [
        report 0.03238
      ]
      report 0.27617
    ]
  ]

  [
    ifelse covid-param > 0 [
      if age-param > 1 and age-param <= 24 [
        report 0.00034
      ]
      if age-param <= 44 [
        report 0.00427
      ]
      if age-param <= 64 [
        report 0.02959
      ]
      report 0.26196
    ]
    [
      if age-param > 1 and age-param <= 24 [
        report 0.00071
      ]
      if age-param <= 44 [
        report 0.00444
      ]
      if age-param <= 64 [
        report 0.01973
      ]
      report 0.24025
    ]
  ]

  report 0
end
@#$#@#$#@
GRAPHICS-WINDOW
433
10
1884
812
-1
-1
13.0
1
10
1
1
1
0
1
1
1
0
110
0
60
0
0
1
ticks
30.0

BUTTON
14
16
77
49
NIL
setup
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

INPUTBOX
130
84
285
144
background-color
0.0
1
0
Color

INPUTBOX
13
172
121
232
house-color-1
15.0
1
0
Color

INPUTBOX
157
172
265
232
house-color-2
55.0
1
0
Color

INPUTBOX
298
175
406
235
house-color-3
25.0
1
0
Color

INPUTBOX
12
248
123
308
house-color-4
35.0
1
0
Color

INPUTBOX
156
250
267
310
house-color-5
45.0
1
0
Color

INPUTBOX
296
251
409
311
house-color-6
105.0
1
0
Color

BUTTON
256
18
319
51
NIL
go
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
344
19
407
52
NIL
go
T
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
101
17
232
50
set default values
default-values
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

SLIDER
9
388
202
421
people-of-house-1
people-of-house-1
0
100
41.0
1
1
people
HORIZONTAL

SLIDER
226
388
419
421
people-of-house-2
people-of-house-2
0
100
44.0
1
1
people
HORIZONTAL

SLIDER
8
433
201
466
people-of-house-3
people-of-house-3
0
100
2.0
1
1
people
HORIZONTAL

SLIDER
226
433
419
466
people-of-house-4
people-of-house-4
0
100
0.0
1
1
people
HORIZONTAL

SLIDER
7
481
202
514
people-of-house-5
people-of-house-5
0
100
0.0
1
1
people
HORIZONTAL

SLIDER
225
478
418
511
people-of-house-6
people-of-house-6
0
100
0.0
1
1
people
HORIZONTAL

BUTTON
64
341
348
374
generate random number of people in houses
generate-random-people-of-houses
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

CHOOSER
101
581
301
626
obstacle
obstacle
"none" "horizontal-lines" "vertical-lines" "horizontal-and-vertical-lines"
3

INPUTBOX
43
638
198
698
horizontal-obstacle-color
125.0
1
0
Color

INPUTBOX
219
638
374
698
vertical-obstacle-color
125.0
1
0
Color

SWITCH
104
725
297
758
generate-age-randomly?
generate-age-randomly?
0
1
-1000

SLIDER
22
769
194
802
minimum-age
minimum-age
1
100
25.0
1
1
NIL
HORIZONTAL

SLIDER
116
811
288
844
starting-age
starting-age
1
100
25.0
1
1
NIL
HORIZONTAL

SLIDER
218
769
390
802
maximum-age
maximum-age
1
100
50.0
1
1
NIL
HORIZONTAL

SWITCH
264
868
479
901
random-number-of-infected?
random-number-of-infected?
1
1
-1000

SLIDER
265
909
509
942
starting-number-of-infected
starting-number-of-infected
1
600
47.0
1
1
people
HORIZONTAL

SLIDER
4
910
256
943
maximum-number-of-infected
maximum-number-of-infected
1
600
30.0
1
1
people
HORIZONTAL

PLOT
622
828
822
978
Level of infection
Time
Agents
0.0
0.0
0.0
600.0
true
true
"" ""
PENS
"Infected" 1.0 0 -2674135 true "" "plot count turtles with [covid > 25]"
"Incubation" 1.0 0 -1184463 true "" "plot count turtles with [covid > 0 and covid <= 25]"
"Health" 1.0 0 -13840069 true "" "plot count turtles with [covid = 0]"
"Dead" 1.0 0 -16777216 true "" "plot dead-agents"

CHOOSER
5
1064
201
1109
gender
gender
"random" "men" "women" "number-of-men" "number-of-women" "random-number-of-men" "random-number-of-women"
6

SLIDER
221
1093
549
1126
max-random-number-of-agents-for-gender
max-random-number-of-agents-for-gender
1
600
46.0
1
1
people
HORIZONTAL

SLIDER
221
1049
476
1082
number-of-agents-for-gender
number-of-agents-for-gender
1
600
50.0
1
1
people
HORIZONTAL

OUTPUT
1131
823
1884
1010
11

MONITOR
950
829
1115
874
Number of women
count turtles with [sex = 1]
17
1
11

MONITOR
840
829
937
874
Number of men
count turtles with [sex = 0]
17
1
11

MONITOR
952
895
1071
940
Infected with covid
count turtles with [covid > 0]
17
1
11

MONITOR
840
896
930
941
Health agents
count turtles with [covid = 0]
17
1
11

CHOOSER
6
1171
144
1216
veil
veil
"random" "all" "no-one" "men" "women"
4

SLIDER
160
1151
421
1184
distance-for-infection-with-veil
distance-for-infection-with-veil
1
20
1.0
1
1
meters
HORIZONTAL

SLIDER
161
1202
440
1235
distance-for-infection-without-veil
distance-for-infection-without-veil
0
100
5.0
1
1
meters
HORIZONTAL

SWITCH
5
869
248
902
go-home-when-agent-is-infected?
go-home-when-agent-is-infected?
0
1
-1000

SLIDER
50
992
224
1025
immunity-after-healing
immunity-after-healing
0
672
165.0
1
1
NIL
HORIZONTAL

SWITCH
86
523
333
556
generate-random-people-position?
generate-random-people-position?
0
1
-1000

SWITCH
4
951
256
984
randomly-generate-infection-rates?
randomly-generate-infection-rates?
1
1
-1000

SLIDER
265
952
445
985
initial-infectivity-rate
initial-infectivity-rate
1
100
50.0
1
1
%
HORIZONTAL

MONITOR
841
955
939
1000
Incubation time
count turtles with [covid > 0 and covid <= 25]
17
1
11

MONITOR
951
955
1061
1000
Infectious agents
count turtles with [covid > 25]
17
1
11

SLIDER
265
993
437
1026
virus-aggression
virus-aggression
1
100
5.0
1
1
%
HORIZONTAL

MONITOR
905
1011
988
1056
Dead agents
dead-agents
17
1
11

@#$#@#$#@
## WHAT IS IT?

(a general understanding of what the model is trying to show or explain)

## HOW IT WORKS

(what rules the agents use to create the overall behavior of the model)

## HOW TO USE IT

(how to use the model, including a description of each of the items in the Interface tab)

## THINGS TO NOTICE

(suggested things for the user to notice while running the model)

## THINGS TO TRY

(suggested things for the user to try to do (move sliders, switches, etc.) with the model)

## EXTENDING THE MODEL

(suggested things to add or change in the Code tab to make the model more complicated, detailed, accurate, etc.)

## NETLOGO FEATURES

(interesting or unusual features of NetLogo that the model uses, particularly in the Code tab; or where workarounds were needed for missing features)

## RELATED MODELS

(models in the NetLogo Models Library and elsewhere which are of related interest)

## CREDITS AND REFERENCES

(a reference to the model's URL on the web if it has one, as well as any other necessary credits, citations, and links)
@#$#@#$#@
default
true
0
Polygon -7500403 true true 150 5 40 250 150 205 260 250

airplane
true
0
Polygon -7500403 true true 150 0 135 15 120 60 120 105 15 165 15 195 120 180 135 240 105 270 120 285 150 270 180 285 210 270 165 240 180 180 285 195 285 165 180 105 180 60 165 15

arrow
true
0
Polygon -7500403 true true 150 0 0 150 105 150 105 293 195 293 195 150 300 150

box
false
0
Polygon -7500403 true true 150 285 285 225 285 75 150 135
Polygon -7500403 true true 150 135 15 75 150 15 285 75
Polygon -7500403 true true 15 75 15 225 150 285 150 135
Line -16777216 false 150 285 150 135
Line -16777216 false 150 135 15 75
Line -16777216 false 150 135 285 75

bug
true
0
Circle -7500403 true true 96 182 108
Circle -7500403 true true 110 127 80
Circle -7500403 true true 110 75 80
Line -7500403 true 150 100 80 30
Line -7500403 true 150 100 220 30

butterfly
true
0
Polygon -7500403 true true 150 165 209 199 225 225 225 255 195 270 165 255 150 240
Polygon -7500403 true true 150 165 89 198 75 225 75 255 105 270 135 255 150 240
Polygon -7500403 true true 139 148 100 105 55 90 25 90 10 105 10 135 25 180 40 195 85 194 139 163
Polygon -7500403 true true 162 150 200 105 245 90 275 90 290 105 290 135 275 180 260 195 215 195 162 165
Polygon -16777216 true false 150 255 135 225 120 150 135 120 150 105 165 120 180 150 165 225
Circle -16777216 true false 135 90 30
Line -16777216 false 150 105 195 60
Line -16777216 false 150 105 105 60

car
false
0
Polygon -7500403 true true 300 180 279 164 261 144 240 135 226 132 213 106 203 84 185 63 159 50 135 50 75 60 0 150 0 165 0 225 300 225 300 180
Circle -16777216 true false 180 180 90
Circle -16777216 true false 30 180 90
Polygon -16777216 true false 162 80 132 78 134 135 209 135 194 105 189 96 180 89
Circle -7500403 true true 47 195 58
Circle -7500403 true true 195 195 58

circle
false
0
Circle -7500403 true true 0 0 300

circle 2
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240

cow
false
0
Polygon -7500403 true true 200 193 197 249 179 249 177 196 166 187 140 189 93 191 78 179 72 211 49 209 48 181 37 149 25 120 25 89 45 72 103 84 179 75 198 76 252 64 272 81 293 103 285 121 255 121 242 118 224 167
Polygon -7500403 true true 73 210 86 251 62 249 48 208
Polygon -7500403 true true 25 114 16 195 9 204 23 213 25 200 39 123

cylinder
false
0
Circle -7500403 true true 0 0 300

dot
false
0
Circle -7500403 true true 90 90 120

face happy
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 255 90 239 62 213 47 191 67 179 90 203 109 218 150 225 192 218 210 203 227 181 251 194 236 217 212 240

face neutral
false
0
Circle -7500403 true true 8 7 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Rectangle -16777216 true false 60 195 240 225

face sad
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 168 90 184 62 210 47 232 67 244 90 220 109 205 150 198 192 205 210 220 227 242 251 229 236 206 212 183

fish
false
0
Polygon -1 true false 44 131 21 87 15 86 0 120 15 150 0 180 13 214 20 212 45 166
Polygon -1 true false 135 195 119 235 95 218 76 210 46 204 60 165
Polygon -1 true false 75 45 83 77 71 103 86 114 166 78 135 60
Polygon -7500403 true true 30 136 151 77 226 81 280 119 292 146 292 160 287 170 270 195 195 210 151 212 30 166
Circle -16777216 true false 215 106 30

flag
false
0
Rectangle -7500403 true true 60 15 75 300
Polygon -7500403 true true 90 150 270 90 90 30
Line -7500403 true 75 135 90 135
Line -7500403 true 75 45 90 45

flower
false
0
Polygon -10899396 true false 135 120 165 165 180 210 180 240 150 300 165 300 195 240 195 195 165 135
Circle -7500403 true true 85 132 38
Circle -7500403 true true 130 147 38
Circle -7500403 true true 192 85 38
Circle -7500403 true true 85 40 38
Circle -7500403 true true 177 40 38
Circle -7500403 true true 177 132 38
Circle -7500403 true true 70 85 38
Circle -7500403 true true 130 25 38
Circle -7500403 true true 96 51 108
Circle -16777216 true false 113 68 74
Polygon -10899396 true false 189 233 219 188 249 173 279 188 234 218
Polygon -10899396 true false 180 255 150 210 105 210 75 240 135 240

house
false
0
Rectangle -7500403 true true 45 120 255 285
Rectangle -16777216 true false 120 210 180 285
Polygon -7500403 true true 15 120 150 15 285 120
Line -16777216 false 30 120 270 120

leaf
false
0
Polygon -7500403 true true 150 210 135 195 120 210 60 210 30 195 60 180 60 165 15 135 30 120 15 105 40 104 45 90 60 90 90 105 105 120 120 120 105 60 120 60 135 30 150 15 165 30 180 60 195 60 180 120 195 120 210 105 240 90 255 90 263 104 285 105 270 120 285 135 240 165 240 180 270 195 240 210 180 210 165 195
Polygon -7500403 true true 135 195 135 240 120 255 105 255 105 285 135 285 165 240 165 195

line
true
0
Line -7500403 true 150 0 150 300

line half
true
0
Line -7500403 true 150 0 150 150

pentagon
false
0
Polygon -7500403 true true 150 15 15 120 60 285 240 285 285 120

person
false
0
Circle -7500403 true true 110 5 80
Polygon -7500403 true true 105 90 120 195 90 285 105 300 135 300 150 225 165 300 195 300 210 285 180 195 195 90
Rectangle -7500403 true true 127 79 172 94
Polygon -7500403 true true 195 90 240 150 225 180 165 105
Polygon -7500403 true true 105 90 60 150 75 180 135 105

plant
false
0
Rectangle -7500403 true true 135 90 165 300
Polygon -7500403 true true 135 255 90 210 45 195 75 255 135 285
Polygon -7500403 true true 165 255 210 210 255 195 225 255 165 285
Polygon -7500403 true true 135 180 90 135 45 120 75 180 135 210
Polygon -7500403 true true 165 180 165 210 225 180 255 120 210 135
Polygon -7500403 true true 135 105 90 60 45 45 75 105 135 135
Polygon -7500403 true true 165 105 165 135 225 105 255 45 210 60
Polygon -7500403 true true 135 90 120 45 150 15 180 45 165 90

sheep
false
15
Circle -1 true true 203 65 88
Circle -1 true true 70 65 162
Circle -1 true true 150 105 120
Polygon -7500403 true false 218 120 240 165 255 165 278 120
Circle -7500403 true false 214 72 67
Rectangle -1 true true 164 223 179 298
Polygon -1 true true 45 285 30 285 30 240 15 195 45 210
Circle -1 true true 3 83 150
Rectangle -1 true true 65 221 80 296
Polygon -1 true true 195 285 210 285 210 240 240 210 195 210
Polygon -7500403 true false 276 85 285 105 302 99 294 83
Polygon -7500403 true false 219 85 210 105 193 99 201 83

square
false
0
Rectangle -7500403 true true 30 30 270 270

square 2
false
0
Rectangle -7500403 true true 30 30 270 270
Rectangle -16777216 true false 60 60 240 240

star
false
0
Polygon -7500403 true true 151 1 185 108 298 108 207 175 242 282 151 216 59 282 94 175 3 108 116 108

target
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240
Circle -7500403 true true 60 60 180
Circle -16777216 true false 90 90 120
Circle -7500403 true true 120 120 60

tree
false
0
Circle -7500403 true true 118 3 94
Rectangle -6459832 true false 120 195 180 300
Circle -7500403 true true 65 21 108
Circle -7500403 true true 116 41 127
Circle -7500403 true true 45 90 120
Circle -7500403 true true 104 74 152

triangle
false
0
Polygon -7500403 true true 150 30 15 255 285 255

triangle 2
false
0
Polygon -7500403 true true 150 30 15 255 285 255
Polygon -16777216 true false 151 99 225 223 75 224

truck
false
0
Rectangle -7500403 true true 4 45 195 187
Polygon -7500403 true true 296 193 296 150 259 134 244 104 208 104 207 194
Rectangle -1 true false 195 60 195 105
Polygon -16777216 true false 238 112 252 141 219 141 218 112
Circle -16777216 true false 234 174 42
Rectangle -7500403 true true 181 185 214 194
Circle -16777216 true false 144 174 42
Circle -16777216 true false 24 174 42
Circle -7500403 false true 24 174 42
Circle -7500403 false true 144 174 42
Circle -7500403 false true 234 174 42

turtle
true
0
Polygon -10899396 true false 215 204 240 233 246 254 228 266 215 252 193 210
Polygon -10899396 true false 195 90 225 75 245 75 260 89 269 108 261 124 240 105 225 105 210 105
Polygon -10899396 true false 105 90 75 75 55 75 40 89 31 108 39 124 60 105 75 105 90 105
Polygon -10899396 true false 132 85 134 64 107 51 108 17 150 2 192 18 192 52 169 65 172 87
Polygon -10899396 true false 85 204 60 233 54 254 72 266 85 252 107 210
Polygon -7500403 true true 119 75 179 75 209 101 224 135 220 225 175 261 128 261 81 224 74 135 88 99

wheel
false
0
Circle -7500403 true true 3 3 294
Circle -16777216 true false 30 30 240
Line -7500403 true 150 285 150 15
Line -7500403 true 15 150 285 150
Circle -7500403 true true 120 120 60
Line -7500403 true 216 40 79 269
Line -7500403 true 40 84 269 221
Line -7500403 true 40 216 269 79
Line -7500403 true 84 40 221 269

wolf
false
0
Polygon -16777216 true false 253 133 245 131 245 133
Polygon -7500403 true true 2 194 13 197 30 191 38 193 38 205 20 226 20 257 27 265 38 266 40 260 31 253 31 230 60 206 68 198 75 209 66 228 65 243 82 261 84 268 100 267 103 261 77 239 79 231 100 207 98 196 119 201 143 202 160 195 166 210 172 213 173 238 167 251 160 248 154 265 169 264 178 247 186 240 198 260 200 271 217 271 219 262 207 258 195 230 192 198 210 184 227 164 242 144 259 145 284 151 277 141 293 140 299 134 297 127 273 119 270 105
Polygon -7500403 true true -1 195 14 180 36 166 40 153 53 140 82 131 134 133 159 126 188 115 227 108 236 102 238 98 268 86 269 92 281 87 269 103 269 113

x
false
0
Polygon -7500403 true true 270 75 225 30 30 225 75 270
Polygon -7500403 true true 30 75 75 30 270 225 225 270
@#$#@#$#@
NetLogo 6.1.1
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
default
0.0
-0.2 0 0.0 1.0
0.0 1 1.0 0.0
0.2 0 0.0 1.0
link direction
true
0
Line -7500403 true 150 150 90 180
Line -7500403 true 150 150 210 180
@#$#@#$#@
0
@#$#@#$#@
